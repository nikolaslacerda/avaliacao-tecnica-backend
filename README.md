# Avaliação Técnica - Grupo Dimed - Backend

Este projeto é resultado de uma avaliação técnica no período de estágio como desenvolvedor no Grupo Dimed.

O projeto consiste em uma API para integrar dados de linhas de ônibus e itinerários com a API do DataPOA, a API permite o CRUD de linhas
de ônibus e de itinerários, além de permitir buscar linhas de ônibus que passem por determinado local (raio). Na API também existe a funcionalidade
de criar e consultar pontos de táxis, com a persistência em memória e em arquivo.

## Justificativa

### Spring Boot

O Spring é um framework open source para a plataforma Java, e foi escolhido para esse projeto pelos seguintes motivos:

- É um framework produtivo que simplifica o desenvolvimento da aplicação, facilitando a finalização do projeto em uma semana, como foi proposto. 
- É o framework utilizado dentro do Grupo Dimed
- Foi o framework em que trabalhamos no período de curso.

### MongoDB

O MongoDB é um banco de dados NoSQL, baseado em documentos, e foi escolhido para esse projeto pelos seguintes motivos:

- MongoDB suporta o modelo reativo e já possui integração com o Spring Webflux, facilitando o desenvolvimento do projeto
- A API do DataPOA possui uma grande quantidade de itens e de listas, por exemplo cada itinerário possui uma lista com uma grande quantidade de coordenadas, o armazenamento em documentos facilita a etapa de persistência, além de visar o futuro escalonamento e o desempenho da aplicação.

## Executando o Projeto

Para executar o projeto basta realizar os seguintes passos:

```bash
# Clone o repositório
$ git clone https://gitlab.com/nikolaslacerda/avaliacao-tecnica-backend.git

# Entre na pasta do projeto
$ cd ./avaliacao-tecnica-backend/backend 

# Execute o build do projeto
$ ./gradlew build

# Execute o projeto
$ docker-compose up

```

> **_AVISO:_** 
> No momento em que a aplicação inicia sua execução, os dados da API do DataPOA são carregados automaticamente para o MongoDB, esse processo demora aproximadamente 2 minutos e 30 segundos, e acontece apenas no processo de build e na inicialização da aplicação. Ao iniciar a aplicação, após a execução desse processo, a mensagem *"Application Initialization Complete"* aparecerá no console, indicando que a aplicação está pronta para uso.

## Documentação

Após a execução da aplicação, a documentação estará disponível em: http://localhost:8080/swagger-ui/

## Exemplos de Requests

Na pasta [postman](postman/) , consta um [arquivo](postman/avaliacao-backend.postman_collection.json) com vários exemplos de requisições para executação.  
Para usar o arquivo basta importá-lo no Postman indo no menu superior, no caminho *File > Import* e selecionar o arquivo disponível.

## Referências

#### Parte 4

- https://ttarnawski.usermd.net/2017/08/17/haversine-formula/
- https://stackoverflow.com/questions/22063842/check-if-a-latitude-and-longitude-is-within-a-circle
- https://stackoverflow.com/questions/21061464/find-geopoints-in-a-radius-near-me

### Parte 5

- https://www.baeldung.com/java-append-to-file
- https://www.baeldung.com/apache-commons-io
- https://kodejava.org/how-do-i-read-text-file-content-line-by-line-using-commons-io/
- https://stackoverflow.com/questions/5175728/how-to-get-the-current-date-time-in-java
- https://stackoverflow.com/questions/51446731/write-to-resource-file-java
- https://stackoverflow.com/questions/3059383/file-path-windows-format-to-java-format

#### Parte 6

- https://stackoverflow.com/questions/43642179/configuring-swagger-ui-with-spring-boot
- https://medium.com/@subenksaha/using-swagger-with-spring-webflux-9f2d9e2874c9
- https://stackoverflow.com/questions/59440108/404-error-with-swagger-ui-and-spring-webflux
- https://stackoverflow.com/questions/62773219/suddenly-springfox-swagger-3-0-is-not-working-with-spring-webflux
- https://kalliphant.com/swagger-spring-rest-controllername-change/
- https://tjf.totvs.com.br/docs/swagger-springfox


#### Parte 7

##### Docker

- https://emmanuelneri.com.br/2017/09/04/executando-aplicacoes-spring-boot-com-docker/
- https://itsromiljain.medium.com/docker-compose-file-to-run-rest-spring-boot-application-container-and-mysql-container-775f15d21416
- https://itsromiljain.medium.com/docker-setup-and-dockerize-an-application-5c24a4c8b428
- https://sairamkrish.medium.com/docker-for-spring-boot-gradle-java-micro-service-done-the-right-way-2f46231dbc06
- https://medium.com/@kartheeksaip/how-to-dockerize-java-spring-boot-api-mongo-db-2ad5561a2592
- https://www.bmc.com/blogs/mongodb-docker-container/
