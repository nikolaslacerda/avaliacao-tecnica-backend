package com.avaliacao.backend.dto;

import com.avaliacao.backend.entity.Linha;
import com.fasterxml.jackson.annotation.JsonProperty;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

public class LinhaDTO {

    @NotBlank
    @Size(min = 2, max = 32)
    @JsonProperty
    private String id;

    @NotBlank
    @Size(min = 2, max = 32)
    @JsonProperty
    private String codigo;

    @NotBlank
    @Size(min = 2, max = 32)
    @JsonProperty
    private String nome;

    public LinhaDTO() {
    }

    public LinhaDTO(String id, String codigo, String nome) {
        this.id = id;
        this.codigo = codigo;
        this.nome = nome;
    }

    public static LinhaDTO convertLinhaToLinhaDto(Linha linha) {
        LinhaDTO linhaDTO = new LinhaDTO();
        linhaDTO.setId(linha.getId());
        linhaDTO.setCodigo(linha.getCodigo());
        linhaDTO.setNome(linha.getNome());
        return linhaDTO;
    }

    public static Linha convertLinhaDtoToLinha(LinhaDTO linhaDto) {
        Linha linha = new Linha();
        linha.setId(linhaDto.getId());
        linha.setCodigo(linhaDto.getCodigo());
        linha.setNome(linhaDto.getNome());
        return linha;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }
}
