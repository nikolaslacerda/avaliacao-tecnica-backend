package com.avaliacao.backend.dto;

import com.avaliacao.backend.entity.Itinerario;
import com.avaliacao.backend.entity.Ponto;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.stream.Collectors;

public class ItinerarioDTO {

    @JsonIgnore
    public String id;

    @NotBlank
    @Size(min = 2, max = 32)
    @JsonProperty
    public String idlinha;

    @NotBlank
    @Size(min = 2, max = 32)
    @JsonProperty
    public String nome;

    @NotBlank
    @Size(min = 2, max = 32)
    @JsonProperty
    public String codigo;

    @NotNull
    @JsonProperty
    public Map<String, Ponto> pontos;

    public ItinerarioDTO() {
    }

    public ItinerarioDTO(String id, String idlinha, String nome, String codigo, Map<String, Ponto> pontos) {
        this.id = id;
        this.idlinha = idlinha;
        this.nome = nome;
        this.codigo = codigo;
        this.pontos = pontos;
    }

    public static ItinerarioDTO convertItinerarioToItinerarioDto(Itinerario itinerario) {
        ItinerarioDTO itinerarioDto = new ItinerarioDTO();
        itinerarioDto.setId(itinerario.getId());
        itinerarioDto.setIdlinha(itinerario.getIdlinha());
        itinerarioDto.setNome(itinerario.getNome());
        itinerarioDto.setCodigo(itinerario.getCodigo());
        itinerarioDto.setPontos(itinerario.getPontos());
        return itinerarioDto;
    }

    public static Itinerario convertItinerarioDtoToItinerario(ItinerarioDTO itinerarioDto) {
        Itinerario itinerario = new Itinerario();
        itinerario.setId(itinerarioDto.getId());
        itinerario.setIdlinha(itinerarioDto.getIdlinha());
        itinerario.setCodigo(itinerarioDto.getCodigo());
        itinerario.setNome(itinerarioDto.getNome());
        itinerario.setPontos(itinerarioDto.getPontos());
        return itinerario;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getIdlinha() {
        return idlinha;
    }

    public void setIdlinha(String idLinha) {
        this.idlinha = idLinha;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public Map<String, Ponto> getPontos() {
        return this.pontos.entrySet().stream()
                .sorted(Comparator.comparingInt(e -> Integer.parseInt(e.getKey())))
                .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue,
                        (oldValue, newValue) -> oldValue, LinkedHashMap::new));
    }

    public void setPontos(Map<String, Ponto> pontos) {
        if(pontos == null){
            this.pontos = new HashMap<>();
        }
        this.pontos = pontos;
    }

    @JsonAnySetter
    public void setItinerario(String name, Ponto value) {
        if(pontos == null){
            this.pontos = new HashMap<>();
        }
        pontos.put(name, value);
    }
}
