package com.avaliacao.backend.dto;

import com.avaliacao.backend.entity.Ponto;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.stream.Collectors;

public class LinhaPontosDTO {

    @JsonProperty("pontos_dentro_do_raio")
    public Map<String, Ponto> pontos = new HashMap<>();
    @JsonProperty
    private String id;
    @JsonProperty
    private String codigo;
    @JsonProperty
    private String nome;

    public LinhaPontosDTO() {
    }

    public LinhaPontosDTO(String id, String codigo, String nome, Map<String, Ponto> pontos) {
        this.id = id;
        this.codigo = codigo;
        this.nome = nome;
        this.pontos = pontos;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Map<String, Ponto> getPontos() {
        return this.pontos.entrySet().stream()
                .sorted(Comparator.comparingInt(e -> Integer.parseInt(e.getKey())))
                .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue,
                        (oldValue, newValue) -> oldValue, LinkedHashMap::new));
    }

    public void setPontos(Map<String, Ponto> pontos) {
        this.pontos = pontos;
    }
}

