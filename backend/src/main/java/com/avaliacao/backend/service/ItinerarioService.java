package com.avaliacao.backend.service;

import com.avaliacao.backend.entity.Itinerario;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public interface ItinerarioService {

    Mono<Itinerario> createItinerario(Itinerario Itinerario);

    Flux<Itinerario> getAllItinerario();

    Mono<Itinerario> getItinerarioByLinha(String idlinha);

    Mono<Itinerario> updateItinerario(String id, Itinerario Itinerario);

    Mono<Void> deleteItinerario(String id);
}
