package com.avaliacao.backend.service;

import com.avaliacao.backend.entity.Taxi;
import com.avaliacao.backend.repository.TaxiRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import javax.validation.Valid;
import java.time.LocalDateTime;

@Service
public class TaxiServiceImpl implements TaxiService {

    private final TaxiRepository taxiRepository;

    public TaxiServiceImpl(TaxiRepository taxiRepository) {
        this.taxiRepository = taxiRepository;
    }

    @Override
    @Transactional
    public Mono<Taxi> createTaxi(Taxi taxi) {
        taxi.setData_hora(LocalDateTime.now());
        return taxiRepository.save(taxi);
    }

    @Override
    public Flux<Taxi> getAllTaxis() {
        return taxiRepository.findAll();
    }

    @Override
    @Transactional
    public Mono<Taxi> updateTaxi(String id, @Valid Taxi taxi) {
        return taxiRepository.findById(id)
                .flatMap(existingTaxi -> {
                    existingTaxi.setNome(taxi.getNome());
                    existingTaxi.setLatitude(taxi.getLatitude());
                    existingTaxi.setLongitude(taxi.getLongitude());
                    return taxiRepository.save(existingTaxi);
                })
                .switchIfEmpty(Mono.error(new Exception("Taxi com o id " + id + " não foi encontrado")));
    }

    @Override
    @Transactional
    public Mono<Void> deleteTaxi(String id) {
        return taxiRepository.findById(id)
                .flatMap(taxiRepository::delete)
                .switchIfEmpty(Mono.error(new Exception("Taxi com o id " + id + " não foi encontrado")));
    }
}
