package com.avaliacao.backend.service;

import com.avaliacao.backend.entity.Itinerario;
import com.avaliacao.backend.exception.ItinerarioNotFoundException;
import com.avaliacao.backend.exception.LinhaIdExistsException;
import com.avaliacao.backend.repository.ItinerarioRepository;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Service
public class ItinerarioServiceImpl implements ItinerarioService {

    private final ItinerarioRepository itinerarioRepository;

    public ItinerarioServiceImpl(ItinerarioRepository itinerarioRepository) {
        this.itinerarioRepository = itinerarioRepository;
    }

    @Override
    public Mono<Itinerario> createItinerario(Itinerario itinerario) {
        return itinerarioRepository.findItinerarioByIdlinha(itinerario.getIdlinha())
                .flatMap(__ -> Mono.error(new LinhaIdExistsException("Itinerario para a linha com o id " + itinerario.getIdlinha() + " já existe")))
                .switchIfEmpty(Mono.defer(() -> itinerarioRepository.save(itinerario)))
                .cast(Itinerario.class);
    }

    @Override
    public Mono<Itinerario> getItinerarioByLinha(String idlinha) {
        return itinerarioRepository.findItinerarioByIdlinha(idlinha);
    }

    @Override
    public Mono<Itinerario> updateItinerario(String idlinha, Itinerario itinerario) {
        return itinerarioRepository.findItinerarioByIdlinha(idlinha)
                .switchIfEmpty(Mono.error(new ItinerarioNotFoundException("Itinerário da linha com o id " + idlinha + " não foi encontrado")))
                .flatMap(existingItinerario -> {
                    existingItinerario.setNome(itinerario.getNome());
                    existingItinerario.setCodigo(itinerario.getCodigo());
                    existingItinerario.setPontos(itinerario.getPontos());
                    return itinerarioRepository.save(existingItinerario);
                });
    }

    @Override
    public Mono<Void> deleteItinerario(String idlinha) {
        return itinerarioRepository.findItinerarioByIdlinha(idlinha)
                .switchIfEmpty(Mono.error(new ItinerarioNotFoundException("Itinerário da linha com o id " + idlinha + " não foi encontrado")))
                .flatMap(itinerarioRepository::delete);
    }

    @Override
    public Flux<Itinerario> getAllItinerario() {
        return itinerarioRepository.findAll();
    }
}
