package com.avaliacao.backend.service;

import com.avaliacao.backend.dto.LinhaPontosDTO;
import com.avaliacao.backend.entity.Itinerario;
import com.avaliacao.backend.entity.Linha;
import com.avaliacao.backend.entity.Ponto;
import com.avaliacao.backend.exception.LinhaIdExistsException;
import com.avaliacao.backend.exception.LinhaNotFoundException;
import com.avaliacao.backend.repository.LinhaRepository;
import com.avaliacao.backend.util.Haversine;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.HashMap;
import java.util.Map;

@Service
public class LinhaServiceImpl implements LinhaService {

    private final LinhaRepository linhaRepository;

    public LinhaServiceImpl(LinhaRepository linhaRepository) {
        this.linhaRepository = linhaRepository;
    }

    @Override
    public Mono<Linha> createLinha(Linha linha) {
        return linhaRepository.findById(linha.getId())
                .flatMap(__ -> Mono.error(new LinhaIdExistsException("Linha com o id " + linha.getId() + " já existe")))
                .switchIfEmpty(Mono.defer(() -> linhaRepository.save(linha)))
                .cast(Linha.class);
    }

    @Override
    public Flux<Linha> getAllLinhas() {
        return linhaRepository.findAll();
    }

    @Override
    public Flux<Linha> getAllLinhasByName(String nome) {
        return linhaRepository.findByNomeContainingIgnoreCase(nome);
    }

    @Override
    public Mono<Linha> updateLinha(String id, Linha linha) {

        return linhaRepository.findById(id)
                .switchIfEmpty(Mono.error(new LinhaNotFoundException("Linha com o id " + id + " não foi encontrada")))
                .flatMap(existingLinha -> {
                    existingLinha.setNome(linha.getNome());
                    existingLinha.setCodigo(linha.getCodigo());
                    return linhaRepository.save(existingLinha);
                });
    }

    @Override
    public Mono<Void> deleteLinha(String id) {
        return linhaRepository.findById(id)
                .switchIfEmpty(Mono.error(new LinhaNotFoundException("Linha com o id " + id + " não foi encontrada")))
                .flatMap(linhaRepository::delete);
    }

    @Override
    public LinhaPontosDTO pontosNoRaio(String lat, String lng, String raio, Itinerario itinerario) {
        Map<String, Ponto> pontosNoRaio = new HashMap<>();

        for (Map.Entry<String, Ponto> ponto : itinerario.getPontos().entrySet()) {
            if (isInsideRadius(lat, lng, ponto.getValue().getLat(), ponto.getValue().getLng(), raio)) {
                pontosNoRaio.put(ponto.getKey(), ponto.getValue());
            }
        }

        LinhaPontosDTO linhaPontosDTO = new LinhaPontosDTO();
        linhaPontosDTO.setId(itinerario.getIdlinha());
        linhaPontosDTO.setNome(itinerario.getNome());
        linhaPontosDTO.setCodigo(itinerario.getCodigo());
        linhaPontosDTO.setPontos(pontosNoRaio);

        return linhaPontosDTO;
    }

    public boolean isInsideRadius(String lat_ponto_1, String lng_ponto_1, String lat_ponto_2, String lng_ponto_2, String raio) {
        double lat_origin = Double.parseDouble(lat_ponto_1);
        double lng_origin = Double.parseDouble(lng_ponto_1);
        double lat_destination = Double.parseDouble(lat_ponto_2);
        double lng_destination = Double.parseDouble(lng_ponto_2);

        double distance = Haversine.getDistance(lat_origin, lng_origin, lat_destination, lng_destination);
        return Double.parseDouble(raio) >= distance;
    }
}
