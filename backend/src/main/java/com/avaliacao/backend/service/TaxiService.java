package com.avaliacao.backend.service;

import com.avaliacao.backend.entity.Taxi;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public interface TaxiService {

    Mono<Taxi> createTaxi(Taxi taxi);

    Flux<Taxi> getAllTaxis();

    Mono<Taxi> updateTaxi(String id, Taxi taxi);

    Mono<Void> deleteTaxi(String id);
}
