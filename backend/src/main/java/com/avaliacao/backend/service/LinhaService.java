package com.avaliacao.backend.service;

import com.avaliacao.backend.dto.LinhaPontosDTO;
import com.avaliacao.backend.entity.Itinerario;
import com.avaliacao.backend.entity.Linha;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public interface LinhaService {

    Mono<Linha> createLinha(Linha linha);

    Flux<Linha> getAllLinhas();

    Flux<Linha> getAllLinhasByName(String nome);

    Mono<Linha> updateLinha(String id, Linha linha);

    Mono<Void> deleteLinha(String id);

    LinhaPontosDTO pontosNoRaio(String lat, String lng, String raio, Itinerario itinerario);
}
