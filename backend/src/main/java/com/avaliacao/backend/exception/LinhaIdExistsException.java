package com.avaliacao.backend.exception;

public class LinhaIdExistsException extends Exception {
    public LinhaIdExistsException(String message) {
        super(message);
    }
}
