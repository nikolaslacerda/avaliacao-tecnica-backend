package com.avaliacao.backend.exception;

public class LinhaNotFoundException extends Exception {
    public LinhaNotFoundException(String message) {
        super(message);
    }
}
