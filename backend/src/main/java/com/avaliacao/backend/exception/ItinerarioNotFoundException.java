package com.avaliacao.backend.exception;

public class ItinerarioNotFoundException extends Exception {
    public ItinerarioNotFoundException(String message) {
        super(message);
    }
}
