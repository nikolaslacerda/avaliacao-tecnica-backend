package com.avaliacao.backend.exception;

import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.http.HttpStatus;

import java.util.Date;
import java.util.List;

public class ExceptionValidationResponse extends ExceptionResponse {

    private List<String> errors;

    public ExceptionValidationResponse(Integer httpStatusCode, HttpStatus httpStatus, String reason, String message, List<String> errors) {
        super(httpStatusCode, httpStatus, reason, message);
        this.errors = errors;
    }

    public List<String> getErrors() {
        return errors;
    }

    public void setErrors(List<String> errors) {
        this.errors = errors;
    }
}
