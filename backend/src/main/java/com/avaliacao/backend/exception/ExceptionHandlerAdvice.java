package com.avaliacao.backend.exception;

import org.springframework.boot.web.servlet.error.ErrorController;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.support.WebExchangeBindException;

import java.util.List;
import java.util.stream.Collectors;

@ControllerAdvice
public class ExceptionHandlerAdvice implements ErrorController {

    public static final String ERROR_PATH = "/error";

    @ExceptionHandler(LinhaNotFoundException.class)
    public ResponseEntity<ExceptionResponse> linhaNotFoundException(LinhaNotFoundException exception) {
        return createHttpResponse(HttpStatus.BAD_REQUEST, exception.getMessage());
    }

    @ExceptionHandler(LinhaIdExistsException.class)
    public ResponseEntity<ExceptionResponse> linhaIdExistsException(LinhaIdExistsException exception) {
        return createHttpResponse(HttpStatus.BAD_REQUEST, exception.getMessage());
    }

    @ExceptionHandler(ItinerarioNotFoundException.class)
    public ResponseEntity<ExceptionResponse> itinerarioNotFoundException(ItinerarioNotFoundException exception) {
        return createHttpResponse(HttpStatus.BAD_REQUEST, exception.getMessage());
    }

    @ExceptionHandler(WebExchangeBindException.class)
    public ResponseEntity<ExceptionValidationResponse> handleWebExchangeBindException(WebExchangeBindException exception) {
        List<String> errors = exception.getBindingResult().getFieldErrors().stream()
                .map(error -> error.getField().toUpperCase() + ": " + error.getDefaultMessage().toUpperCase())
                .collect(Collectors.toList());
        return createHttpValidationResponse(HttpStatus.BAD_REQUEST, "Invalid field(s)", errors);
    }

    private ResponseEntity<ExceptionResponse> createHttpResponse(HttpStatus httpStatus, String message) {
        return new ResponseEntity<>(new ExceptionResponse(httpStatus.value(), httpStatus, httpStatus.getReasonPhrase().toUpperCase(), message.toUpperCase()), httpStatus);
    }

    private ResponseEntity<ExceptionValidationResponse> createHttpValidationResponse(HttpStatus httpStatus, String message, List<String> errors) {
        return new ResponseEntity<>(new ExceptionValidationResponse(httpStatus.value(), httpStatus, httpStatus.getReasonPhrase().toUpperCase(), message.toUpperCase(), errors), httpStatus);
    }

    @Override
    public String getErrorPath() {
        return ERROR_PATH;
    }
}
