package com.avaliacao.backend.config;

import com.avaliacao.backend.BackendApplication;
import com.avaliacao.backend.dto.ItinerarioDTO;
import com.avaliacao.backend.entity.Itinerario;
import com.avaliacao.backend.entity.Linha;
import com.avaliacao.backend.entity.Taxi;
import com.avaliacao.backend.repository.ItinerarioRepository;
import com.avaliacao.backend.repository.LinhaRepository;
import com.avaliacao.backend.repository.TaxiRepository;
import com.avaliacao.backend.util.TaxiFile;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.data.mongodb.core.ReactiveMongoOperations;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;
import reactor.core.publisher.Flux;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import static com.avaliacao.backend.constant.ApiConstant.GET_ITINERARIO_URL;
import static com.avaliacao.backend.constant.ApiConstant.GET_LINHAS_URL;
import static com.avaliacao.backend.dto.ItinerarioDTO.convertItinerarioDtoToItinerario;

@Configuration
public class IntegrationConfig {

    public static final Logger log = LoggerFactory.getLogger(BackendApplication.class);

    private final RestTemplate restTemplate;

    public IntegrationConfig(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    @Bean
    CommandLineRunner init(
            ReactiveMongoOperations operations,
            LinhaRepository linhaRepository,
            ItinerarioRepository itinerarioRepository,
            TaxiRepository taxiRepository)
            throws MalformedURLException, InterruptedException {

        List<Linha> linhas = getAllLinhasFromApi();
        List<Taxi> taxis = getAllTaxisFromFile();
        List<Itinerario> itinerarios = getAllItinerariosByLinhaFromApi();

        return args -> {
            Flux<Linha> linhaFlux = Flux.fromIterable(linhas)
                    .flatMap(linhaRepository::save);

            linhaFlux.thenMany(linhaRepository.findAll())
                    .subscribe();

            Flux<Itinerario> itinerarioFlux = Flux.fromIterable(itinerarios)
                    .flatMap(itinerarioRepository::save);

            itinerarioFlux.thenMany(itinerarioRepository.findAll())
                    .subscribe();

            Flux<Taxi> taxiFlux = Flux.fromIterable(taxis)
                    .flatMap(taxiRepository::save);

            taxiFlux.thenMany(taxiRepository.findAll())
                    .subscribe();

            log.info("[INFO] Application Initialization Complete");
        };
    }

    public List<Linha> getAllLinhasFromApi() {
        log.info("[INFO] Starting linhas integration...");
        ResponseEntity<List<Linha>> response = restTemplate.exchange(GET_LINHAS_URL,
                HttpMethod.GET, null, new ParameterizedTypeReference<>() {
                });
        return response.getBody();
    }

    public List<Itinerario> getAllItinerariosByLinhaFromApi() throws MalformedURLException, InterruptedException {
        log.info("[INFO] Starting itinerarios integration...");
        List<Linha> linhas = getAllLinhasFromApi();
        List<Itinerario> itinerarios = new ArrayList<>();
        ObjectMapper mapper = new ObjectMapper();

        for (Linha linha : linhas) {
            TimeUnit.MILLISECONDS.sleep(100);
            URL getItinerarioUrl = new URL(GET_ITINERARIO_URL + linha.getId());
            try {
                ItinerarioDTO itinerarioDto = mapper.readValue(getItinerarioUrl, ItinerarioDTO.class);
                Itinerario itinerario = convertItinerarioDtoToItinerario(itinerarioDto);
                itinerarios.add(itinerario);
            } catch (IOException e) {
                log.error("[ERROR] error in load itinerario from linha with id = " + linha.getId() + ", error => " + e);
            }
        }
        return itinerarios;
    }

    public List<Taxi> getAllTaxisFromFile() {
        log.info("[INFO] Starting load taxis from file...");
        return TaxiFile.getTaxis();
    }


}
