package com.avaliacao.backend.resource;

import com.avaliacao.backend.entity.Taxi;
import com.avaliacao.backend.service.TaxiService;
import com.avaliacao.backend.util.TaxiFile;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import javax.validation.Valid;

@Api(value = "Taxi", tags = {"Taxi API"})
@RestController
@RequestMapping("api/taxis")
public class TaxiResource {

    private final TaxiService taxiService;

    public TaxiResource(TaxiService taxiService) {
        this.taxiService = taxiService;
    }

    @ApiOperation(value = "Lista todos os pontos de taxis", notes = "Este endpoint lista todos os dados cadastrados na aplicação")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Listagem efetuada com sucesso")
    })
    @GetMapping
    public Flux<Taxi> getTaxis() {
        return taxiService.getAllTaxis();
    }

    @ApiOperation(value = "Cria um novo ponto de taxi", notes = "Este endpoint cadastra um novo ponto de taxi")
    @ApiResponses({
            @ApiResponse(code = 201, message = "Criação efetuada com sucesso")
    })
    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Mono<Taxi> saveTaxi(@Valid @RequestBody Taxi taxi) {
        return taxiService.createTaxi(taxi).doOnSuccess(TaxiFile::insertTaxi);
    }

    @ApiOperation(value = "Lista o conteudo do arquivo dos pontos de taxi", notes = "Este endpoint lista o conteudo do arquivo dos pontos de taxi, para não ser necessário abri-lo em disco")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Listagem efetuada com sucesso")
    })
    @GetMapping("/file")
    public String printFile() {
        return TaxiFile.printFile();
    }
}
