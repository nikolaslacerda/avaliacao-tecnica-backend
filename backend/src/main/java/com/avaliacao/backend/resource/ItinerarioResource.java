package com.avaliacao.backend.resource;

import com.avaliacao.backend.dto.ItinerarioDTO;
import com.avaliacao.backend.entity.Itinerario;
import com.avaliacao.backend.service.ItinerarioService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Mono;

import javax.validation.Valid;

@Api(value = "Itinerários", tags = {"Itinerários API"})
@RestController
@RequestMapping("api/itinerario")
public class ItinerarioResource {

    private final ItinerarioService itinerarioService;

    public ItinerarioResource(ItinerarioService itinerarioService) {
        this.itinerarioService = itinerarioService;
    }

    @ApiOperation(value = "Cria um novo itinerário referente a uma linha de ônibus", notes = "Este endpoint cria um novo itinerário referente a uma linha de ônibus na aplicação")
    @ApiResponses({@ApiResponse(code = 201, message = "Criação efetuada com sucesso")})
    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Mono<ItinerarioDTO> saveItinerario(@Valid @RequestBody ItinerarioDTO itinerarioDto) {
        Itinerario itinerario = ItinerarioDTO.convertItinerarioDtoToItinerario(itinerarioDto);
        return itinerarioService.createItinerario(itinerario).map(ItinerarioDTO::convertItinerarioToItinerarioDto);
    }

    @ApiOperation(value = "Lista o itinerário de uma determinada linha de ônibus", notes = "Este endpoint lista o itinerário de uma determinada linha de ônibus")
    @ApiResponses({@ApiResponse(code = 200, message = "Listagem efetuada com sucesso")})
    @GetMapping
    public Mono<ItinerarioDTO> getItinerarioByLinha(@RequestParam(name = "linha") String idlinha) {
        return itinerarioService.getItinerarioByLinha(idlinha).map(ItinerarioDTO::convertItinerarioToItinerarioDto);
    }

    @ApiOperation(value = "Atualiza o itinerário de uma linha de ônibus", notes = "Este endpoint atualiza o itinerário de uma determinada linha de ônibus da aplicação")
    @ApiResponses({@ApiResponse(code = 201, message = "Atualização efetuada com sucesso")})
    @PutMapping("{idlinha}")
    @ResponseStatus(code = HttpStatus.CREATED)
    public Mono<ItinerarioDTO> updateItinerario(@PathVariable(value = "idlinha") String idlinha, @Valid @RequestBody ItinerarioDTO itinerarioDto) {
        Itinerario itinerario = ItinerarioDTO.convertItinerarioDtoToItinerario(itinerarioDto);
        return itinerarioService.updateItinerario(idlinha, itinerario).map(ItinerarioDTO::convertItinerarioToItinerarioDto);
    }

    @ApiOperation(value = "Remove o itinerário de uma linha de ônibus", notes = "Este endpoint remove o itinerário de uma determinada linha de ônibus da aplicação")
    @ApiResponses({@ApiResponse(code = 204, message = "Remoção efetuada com sucesso")})
    @DeleteMapping("{idlinha}")
    @ResponseStatus(code = HttpStatus.NO_CONTENT)
    public Mono<Void> deleteItinerario(@PathVariable(value = "idlinha") String idlinha) {
        return itinerarioService.deleteItinerario(idlinha);
    }
}
