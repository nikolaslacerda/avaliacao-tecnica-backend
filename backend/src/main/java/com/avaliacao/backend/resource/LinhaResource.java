package com.avaliacao.backend.resource;

import com.avaliacao.backend.dto.LinhaDTO;
import com.avaliacao.backend.dto.LinhaPontosDTO;
import com.avaliacao.backend.entity.Linha;
import com.avaliacao.backend.service.ItinerarioService;
import com.avaliacao.backend.service.LinhaService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import javax.validation.Valid;
import java.util.Optional;

@Api(value = "Linhas de Ônibus", tags = {"Linhas de Ônibus API"})
@RestController
@RequestMapping("api/linhas")
public class LinhaResource {

    private final LinhaService linhaService;
    private final ItinerarioService itinerarioService;

    public LinhaResource(LinhaService linhaService, ItinerarioService itinerarioService) {
        this.linhaService = linhaService;
        this.itinerarioService = itinerarioService;
    }

    @ApiOperation(value = "Lista as linhas de ônibus filtradas por nome, caso não seja determinado um nome, listará todas as linhas", notes = "Este endpoint lista as linhas de ônibus filtradas por nome, caso não seja determinado um nome, o endpoint listará todas as linhas de ônibus cadastradas na aplicação")
    @ApiResponses({@ApiResponse(code = 200, message = "Listagem efetuada com sucesso")})
    @GetMapping
    public Flux<LinhaDTO> getLinhas(@RequestParam(name = "nome", required = false) Optional<String> nome) {
        return nome.map(s -> linhaService.getAllLinhasByName(s).map(LinhaDTO::convertLinhaToLinhaDto).switchIfEmpty(Flux.empty()))
                .orElseGet(() -> linhaService.getAllLinhas().map(LinhaDTO::convertLinhaToLinhaDto).switchIfEmpty(Flux.empty()));
    }

    @ApiOperation(value = "Lista todas as linhas de ônibus que passam em um determinado raio", notes = "Este endpoint lista todas as linhas de ônibus que passam em um determinado raio")
    @ApiResponses({@ApiResponse(code = 200, message = "Listagem efetuada com sucesso")})
    @GetMapping("/search")
    public Flux<LinhaPontosDTO> getLinhasNoRaio(@RequestParam(name = "lat") String lat, @RequestParam(name = "lng") String lng, @RequestParam(name = "raio") String raio) {
        return itinerarioService.getAllItinerario().map(itinerario -> linhaService.pontosNoRaio(lat, lng, raio, itinerario)).filter(linha -> !linha.getPontos().isEmpty());
    }

    @ApiOperation(value = "Cria uma nova linha de ônibus", notes = "Este endpoint cria uma nova linha de ônibus na aplicação")
    @ApiResponses({@ApiResponse(code = 201, message = "Criação efetuada com sucesso")})
    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Mono<LinhaDTO> saveLinha(@Valid @RequestBody LinhaDTO linhaDto) {
        Linha linha = LinhaDTO.convertLinhaDtoToLinha(linhaDto);
        return linhaService.createLinha(linha).map(LinhaDTO::convertLinhaToLinhaDto);
    }

    @ApiOperation(value = "Atualiza uma linha de ônibus", notes = "Este endpoint atualiza uma determinada linha de ônibus da aplicação")
    @ApiResponses({@ApiResponse(code = 201, message = "Atualização efetuada com sucesso")})
    @PutMapping("{id}")
    @ResponseStatus(code = HttpStatus.CREATED)
    public Mono<LinhaDTO> updateLinha(@PathVariable(value = "id") String id, @Valid @RequestBody LinhaDTO linhaDto) {
        Linha linha = LinhaDTO.convertLinhaDtoToLinha(linhaDto);
        return linhaService.updateLinha(id, linha).map(LinhaDTO::convertLinhaToLinhaDto);
    }

    @ApiOperation(value = "Remove uma linha de ônibus", notes = "Este endpoint remove uma determinada linha de ônibus da aplicação")
    @ApiResponses({@ApiResponse(code = 204, message = "Remoção efetuada com sucesso")})
    @DeleteMapping("{id}")
    @ResponseStatus(code = HttpStatus.NO_CONTENT)
    public Mono<Void> deleteLinha(@PathVariable(value = "id") String id) {
        return linhaService.deleteLinha(id);
    }
}
