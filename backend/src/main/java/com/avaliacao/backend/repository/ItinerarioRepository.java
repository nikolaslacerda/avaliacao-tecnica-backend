package com.avaliacao.backend.repository;

import com.avaliacao.backend.entity.Itinerario;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import reactor.core.publisher.Mono;

public interface ItinerarioRepository extends ReactiveMongoRepository<Itinerario, String> {

    Mono<Itinerario> findItinerarioByIdlinha(String idlinha);
}
