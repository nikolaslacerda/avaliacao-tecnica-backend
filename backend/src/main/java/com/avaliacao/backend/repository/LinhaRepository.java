package com.avaliacao.backend.repository;

import com.avaliacao.backend.entity.Linha;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import reactor.core.publisher.Flux;

public interface LinhaRepository extends ReactiveMongoRepository<Linha, String> {

    Flux<Linha> findByNomeContainingIgnoreCase(String nome);
}
