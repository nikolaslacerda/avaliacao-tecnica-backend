package com.avaliacao.backend.repository;

import com.avaliacao.backend.entity.Taxi;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;

public interface TaxiRepository extends ReactiveMongoRepository<Taxi, String> {
}
