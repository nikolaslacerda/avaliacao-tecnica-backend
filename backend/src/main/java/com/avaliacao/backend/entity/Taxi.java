package com.avaliacao.backend.entity;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.time.LocalDateTime;

@Document(collection = "taxis")
public class Taxi {

    @Id
    private String id;

    @NotBlank
    @Size(min = 2, max = 32)
    private String nome;

    @NotBlank
    @Size(min = 3, max = 25)
    private String latitude;

    @NotBlank
    @Size(min = 3, max = 25)
    private String longitude;

    private LocalDateTime data_hora;

    public Taxi() {
    }

    public Taxi(String id, String nome, String latitude, String longitude, LocalDateTime data_hora) {
        this.id = id;
        this.nome = nome;
        this.latitude = latitude;
        this.longitude = longitude;
        this.data_hora = data_hora;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public LocalDateTime getData_hora() {
        return data_hora;
    }

    public void setData_hora(LocalDateTime data_hora) {
        this.data_hora = data_hora;
    }

    @Override
    public String toString() {
        return "Taxi {" +
                "id = '" + id + '\'' +
                ", nome = '" + nome + '\'' +
                ", latitude = '" + latitude + '\'' +
                ", longitude = '" + longitude + '\'' +
                ", data_hora = '" + data_hora + '\'' +
                '}';
    }
}
