package com.avaliacao.backend.entity;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@Document(collection = "linhas")
public class Linha {

    @Id
    private String id;

    @NotBlank
    @Size(min = 2, max = 32)
    private String codigo;

    @NotBlank
    @Size(min = 2, max = 32)
    private String nome;

    public Linha() {
    }

    public Linha(String id, String codigo, String nome) {
        this.id = id;
        this.codigo = codigo;
        this.nome = nome;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    @Override
    public String toString() {
        return "Linha {" +
                "id = '" + id + '\'' +
                ", codigo = '" + codigo + '\'' +
                ", nome = '" + nome + '\'' +
                '}';
    }
}
