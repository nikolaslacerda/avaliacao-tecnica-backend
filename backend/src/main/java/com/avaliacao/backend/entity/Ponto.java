package com.avaliacao.backend.entity;

public class Ponto {

    private String lat;
    private String lng;

    public Ponto() {
    }

    public Ponto(String lat, String lng) {
        this.lat = lat;
        this.lng = lng;
    }

    public String getLat() {
        return lat;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    public String getLng() {
        return lng;
    }

    public void setLng(String lng) {
        this.lng = lng;
    }

    @Override
    public String toString() {
        return "Ponto {" +
                "lat = '" + lat + '\'' +
                ", lng = '" + lng + '\'' +
                '}';
    }
}
