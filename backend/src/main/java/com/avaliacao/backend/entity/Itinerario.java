package com.avaliacao.backend.entity;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.HashMap;
import java.util.Map;

@Document(collection = "itinerarios")
public class Itinerario {

    @Id
    private String id;

    @NotBlank
    @Size(min = 2, max = 32)
    private String idlinha;

    @NotBlank
    @Size(min = 2, max = 32)
    private String nome;

    @NotBlank
    @Size(min = 2, max = 32)
    private String codigo;

    private Map<String, Ponto> pontos = new HashMap<>();

    public Itinerario() {
    }

    public Itinerario(String id, String idlinha, String nome, String codigo, Map<String, Ponto> pontos) {
        this.id = id;
        this.idlinha = idlinha;
        this.nome = nome;
        this.codigo = codigo;
        this.pontos = pontos;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getIdlinha() {
        return idlinha;
    }

    public void setIdlinha(String idLinha) {
        this.idlinha = idLinha;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String código) {
        this.codigo = código;
    }

    public Map<String, Ponto> getPontos() {
        return this.pontos;
    }

    public void setPontos(Map<String, Ponto> pontos) {
        this.pontos = pontos;
    }

    @Override
    public String toString() {
        return "Itinerario {" +
                "id = " + id +
                ", idlinha = '" + idlinha + '\'' +
                ", nome = '" + nome + '\'' +
                ", codigo = '" + codigo + '\'' +
                ", pontos = " + pontos +
                '}';
    }
}
