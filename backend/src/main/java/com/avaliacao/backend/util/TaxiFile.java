package com.avaliacao.backend.util;

import com.avaliacao.backend.BackendApplication;
import com.avaliacao.backend.entity.Taxi;
import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

public class TaxiFile {

    public static final Logger log = LoggerFactory.getLogger(BackendApplication.class);

    private static final String FILENAME = "data.txt";

    public static void insertTaxi(Taxi taxi) {
        log.info("[INFO] Insert taxi into file");
        String data = taxi.getNome() + "#" + taxi.getLatitude() + "#" + taxi.getLongitude() + "#" + taxi.getData_hora() + "\r\n";
        try {
            File file = new File(FILENAME);
            FileUtils.writeStringToFile(file, data, StandardCharsets.UTF_8, true);
            log.info("[INFO] Insert taxi successfully");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static List<Taxi> getTaxis() {
        List<Taxi> taxis = new ArrayList<>();
        try {
            File file = new File(FILENAME);

            if (!file.exists()) {
                log.info("[INFO] Create file in " + file.getAbsolutePath() + " to insert taxis in system");
                file.createNewFile();
                FileUtils.writeStringToFile(file, "PONTO-ZONA-SUL-1#-30.12373379817800000#-51.22266028234100000#2019-02-10T16:14:34.828\r\n", StandardCharsets.UTF_8, true);
                FileUtils.writeStringToFile(file, "PONTO-ZONA-NORTE-1#-30.0103346#-51.1724526#2019-03-10T16:14:34.828\r\n", StandardCharsets.UTF_8, true);
            }

            List<String> lines = FileUtils.readLines(file, "UTF-8");
            lines.forEach(line -> {
                String[] separate_items = line.split("#");
                Taxi taxi = new Taxi();
                taxi.setNome(separate_items[0]);
                taxi.setLatitude(separate_items[1]);
                taxi.setLongitude(separate_items[2]);
                taxi.setData_hora(LocalDateTime.parse(separate_items[3]));
                taxis.add(taxi);
            });
        } catch (IOException e) {
            System.out.println(e);
        }
        return taxis;
    }

    public static String printFile() {
        log.info("[INFO] Print taxi file");
        StringBuilder data = new StringBuilder();
        try {
            File file = new File(FILENAME);
            List<String> lines = FileUtils.readLines(file, "UTF-8");
        lines.forEach(line -> data.append(line).append("\n"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        return data.toString();
    }
}
