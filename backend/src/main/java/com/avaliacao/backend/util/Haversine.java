package com.avaliacao.backend.util;

public class Haversine {

    public static final int EARTH_MEAN_RADIUS = 6371;

    public static double getDistance(double startLatitude, double startLongitude, double endLatitude, double endLongitude) {
        return distHaversine(startLatitude, startLongitude, endLatitude, endLongitude);
    }

    private static double getSpanInRadians(double max, double min) {
        return Math.toRadians(max - min);
    }

    private static double distHaversine(double lat1, double lng1, double lat2, double lng2) {
        double dLat = getSpanInRadians(lat2, lat1);
        double dLng = getSpanInRadians(lng2, lng1);

        lat1 = Math.toRadians(lat1);
        lat2 = Math.toRadians(lat2);

        double a = Math.sin(dLat / 2) * Math.sin(dLat / 2) + Math.cos(lat1) * Math.cos(lat2) * Math.sin(dLng / 2) * Math.sin(dLng / 2);
        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
        double d = EARTH_MEAN_RADIUS * c;

        return Math.round(d * 1000) / 1000.0;
    }
}
