package com.avaliacao.backend.resource;


import com.avaliacao.backend.entity.Itinerario;
import com.avaliacao.backend.entity.Linha;
import com.avaliacao.backend.service.ItinerarioService;
import com.avaliacao.backend.service.LinhaService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.reactive.WebFluxTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.reactive.server.WebTestClient;
import reactor.core.publisher.Mono;

import java.util.Collections;
import java.util.HashMap;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@WebFluxTest(ItinerarioResource.class)
public class ItinerarioResourceTest {

    @MockBean
    ItinerarioService itinerarioService;

    @Autowired
    private WebTestClient webTestClient;

    @Test
    public void shouldCreateItinerario() {
        Itinerario itinerario = new Itinerario(null, "9999", "LINHA TEST", "EXEMPLO", new HashMap<>());

        Mono<Itinerario> itinerarioMono = Mono.just(itinerario);
        when(itinerarioService.createItinerario(any())).thenReturn(itinerarioMono);

        webTestClient.post().uri("/api/itinerario")
                .contentType(MediaType.APPLICATION_JSON)
                .body(Mono.just(itinerario), Itinerario.class)
                .exchange()
                .expectStatus().isCreated()
                .expectHeader().contentType(MediaType.APPLICATION_JSON)
                .expectBody()
                .jsonPath("$.idlinha").isEqualTo("9999")
                .jsonPath("$.codigo").isEqualTo("EXEMPLO")
                .jsonPath("$.nome").isEqualTo("LINHA TEST");
    }

    @Test
    public void shouldGetItinerarioByLinhaId() {
        Itinerario itinerario = new Itinerario(null, "9999", "LINHA TEST", "EXEMPLO", new HashMap<>());

        Mono<Itinerario> itinerarioMono = Mono.just(itinerario);
        when(itinerarioService.getItinerarioByLinha("9999")).thenReturn(itinerarioMono);

        webTestClient.get().uri("/api/itinerario?linha={idlinha}", Collections.singletonMap("idlinha", "9999"))
                .exchange()
                .expectStatus().isOk()
                .expectHeader().contentType(MediaType.APPLICATION_JSON)
                .expectBody()
                .jsonPath("$.idlinha").isEqualTo("9999")
                .jsonPath("$.codigo").isEqualTo("EXEMPLO")
                .jsonPath("$.nome").isEqualTo("LINHA TEST");
    }

    @Test
    public void shouldUpdateItinerario() {
        Itinerario itinerario = new Itinerario(null, "9999", "LINHA UPDATED", "EXEMPLO", new HashMap<>());

        Mono<Itinerario> itinerarioMono = Mono.just(itinerario);
        when(itinerarioService.updateItinerario(any(), any())).thenReturn(itinerarioMono);

        webTestClient.put().uri("/api/itinerario/{idlinha}", Collections.singletonMap("idlinha", "9999"))
                .contentType(MediaType.APPLICATION_JSON)
                .body(Mono.just(itinerario), Itinerario.class)
                .exchange()
                .expectStatus().isCreated()
                .expectHeader().contentType(MediaType.APPLICATION_JSON)
                .expectBody()
                .jsonPath("$.idlinha").isEqualTo("9999")
                .jsonPath("$.codigo").isEqualTo("EXEMPLO")
                .jsonPath("$.nome").isEqualTo("LINHA UPDATED");
    }

    @Test
    public void shoudDeleteItinerario() {
        when(itinerarioService.deleteItinerario("9999")).thenReturn(Mono.empty());

        webTestClient.delete()
                .uri("/api/itinerario/{idlinha}", Collections.singletonMap("idlinha", "9999"))
                .exchange()
                .expectStatus().isNoContent()
                .expectBody(Void.class);
    }

}
