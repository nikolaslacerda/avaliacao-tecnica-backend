package com.avaliacao.backend.resource;

import com.avaliacao.backend.entity.Linha;
import com.avaliacao.backend.service.ItinerarioService;
import com.avaliacao.backend.service.LinhaService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.reactive.WebFluxTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.reactive.server.WebTestClient;
import reactor.core.publisher.Mono;

import java.util.Collections;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@WebFluxTest(LinhaResource.class)
public class LinhaResourceTest {

    @MockBean
    LinhaService linhaService;

    @MockBean
    ItinerarioService itinerarioService;

    @Autowired
    private WebTestClient webTestClient;

    @Test
    public void shouldCreateLinha() {
        Linha linha = new Linha("9999", "EXEMPLO", "LINHA TEST");

        Mono<Linha> linhaMono = Mono.just(linha);
        when(linhaService.createLinha(any())).thenReturn(linhaMono);

        webTestClient.post().uri("/api/linhas")
                .contentType(MediaType.APPLICATION_JSON)
                .body(Mono.just(linha), Linha.class)
                .exchange()
                .expectStatus().isCreated()
                .expectHeader().contentType(MediaType.APPLICATION_JSON)
                .expectBody()
                .jsonPath("$.id").isEqualTo("9999")
                .jsonPath("$.codigo").isEqualTo("EXEMPLO")
                .jsonPath("$.nome").isEqualTo("LINHA TEST");
    }

    @Test
    public void shouldUpdateLinha() {
        Linha linha = new Linha("9999", "EXAMPLE", "LINHA UPDATED");

        Mono<Linha> linhaMono = Mono.just(linha);
        when(linhaService.updateLinha(any(), any())).thenReturn(linhaMono);

        webTestClient.put().uri("/api/linhas/{id}", Collections.singletonMap("id", "9999"))
                .contentType(MediaType.APPLICATION_JSON)
                .body(Mono.just(linha), Linha.class)
                .exchange()
                .expectStatus().isCreated()
                .expectHeader().contentType(MediaType.APPLICATION_JSON)
                .expectBody()
                .jsonPath("$.id").isEqualTo("9999")
                .jsonPath("$.codigo").isEqualTo("EXAMPLE")
                .jsonPath("$.nome").isEqualTo("LINHA UPDATED");
    }

    @Test
    public void shoudDeleteLinha() {
        when(linhaService.deleteLinha("9999")).thenReturn(Mono.empty());

        webTestClient.delete()
                .uri("/api/linhas/{id}", Collections.singletonMap("id", "9999"))
                .exchange()
                .expectStatus().isNoContent()
                .expectBody(Void.class);
    }
}
